﻿using Microsoft.Data.SqlClient;
using NotItunes.Models;
using NotItunes.Repositories;
using System;
using System.Collections.Generic;

namespace NotItunes
{
    class Program
    {

        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();

            TestAllFunctionality(repository);
            TestSpecificCustomerByName(repository);
            TestSpecificCustomerById(repository);
            TestInsert(repository);
            TestUpdate(repository);
            TestLimitOffset(repository);
            TestNumberOfCustomerInCountry(repository);
            TestCustomerSpending(repository);
            TestMostPopularGenre(repository);
        }

        static void TestAllFunctionality(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        static void TestSpecificCustomerByName(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetSpecificCustomerByName("John"));
        }

        static void TestSpecificCustomerById(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerById(2));
        }

        static void TestInsert(ICustomerRepository repository)
        {
            Customer c = new Customer();
            c.FirstName = "Sarah";
            c.LastName = "Eggington";
            c.Country = "England";
            c.PostalCode = "3432";
            c.Phone = "458734944";
            c.Email = "Ladyperson@hotmail.com";

            Console.WriteLine("Customer was added(True/False): " + repository.AddCustomer(c));
        }

        static void TestUpdate(ICustomerRepository repository)
        {
            Customer c = new Customer();
            c.ID = 2;
            c.FirstName = "Sarah";
            c.LastName = "Hurrdurr";
            c.Country = "Sweden";
            c.PostalCode = "3432";
            c.Phone = "458734944";
            c.Email = "Ladyperson@gmail.com";

            Console.WriteLine("Customer was updated(True/False): " + repository.UpdateCustomer(c));
        }

        static void TestLimitOffset(ICustomerRepository repository)
        {
            PrintCustomers(repository.PageOfCustomers(5, 10));
        }

        static void TestNumberOfCustomerInCountry(ICustomerRepository repository)
        {
            PrintCustomers(repository.ReturnCustomersByCountry());
        }

        static void TestCustomerSpending(ICustomerRepository repository)
        {
            PrintCustomers(repository.ReturnCustomersByInvoiceTotal());
        }

        static void TestMostPopularGenre(ICustomerRepository repository)
        {
            Customer c = new Customer();
            c.FirstName = "Roberto";
            c.LastName = "Almeida";

            PrintCustomers(repository.ReturnMostPopularGenre(c));
        }

        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
        static void PrintCustomers(IEnumerable<CustomerCountry> customersCountrys)
        {
            foreach (CustomerCountry customerCountry in customersCountrys)
            {
                PrintCustomer(customerCountry);
            }
        }
        static void PrintCustomers(IEnumerable<CustomerSpender> customersSpenders)
        {
            foreach (CustomerSpender customerSpender in customersSpenders)
            {
                PrintCustomer(customerSpender);
            }
        }
        static void PrintCustomers(IEnumerable<CustomerGenre> customersGenres)
        {
            foreach (CustomerGenre customerGenre in customersGenres)
            {
                PrintCustomer(customerGenre);
            }
        }
        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- {customer.ID} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email}---");
        }
        static void PrintCustomer(CustomerCountry customerCountry)
        {

            Console.WriteLine($"--- {customerCountry.Country} {customerCountry.Customers}---");
        }
        static void PrintCustomer(CustomerSpender customerSpender)
        {
            Console.WriteLine($"--- {customerSpender.FirstName} {customerSpender.LastName} {customerSpender.TotalSpending}---");
        }
        static void PrintCustomer(CustomerGenre customerGenre)
        {
            Console.WriteLine($"--- {customerGenre.FirstName} {customerGenre.LastName} {customerGenre.Genre} {customerGenre.TotalPurchases}---");
        }
    }
}
