﻿
using System.Data.SqlTypes;

namespace NotItunes.Models
{
    public class CustomerSpender
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public SqlDecimal TotalSpending { get; set; }
        public CustomerSpender()
        {}
    }
}
