﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotItunes.Models
{
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int Customers { get; set; }
        public CustomerCountry()
        {}
    }
}
