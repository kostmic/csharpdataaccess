﻿

namespace NotItunes.Models
{
    public class CustomerGenre
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Genre { get; set; }
        public int TotalPurchases { get; set; }
        public CustomerGenre()
        {}
    }
}
