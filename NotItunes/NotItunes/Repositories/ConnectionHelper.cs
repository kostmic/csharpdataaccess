﻿
using Microsoft.Data.SqlClient;

namespace NotItunes.Repositories
{
   public class ConnectionHelper
    {
        /// <summary>
        /// Should be passed to a new instance of SqlConnection. 
        /// Class contains path and name of which database to connect to. Change values as neccessary.
        /// </summary>
        /// <returns></returns>
        public static string GetConncetionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            //builder.DataSource = @"ND-5CG92747K8\SQLEXPRESS01";
            connectionStringBuilder.DataSource = @"ND-5CG9030M7C\SQLEXPRESS";
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            return connectionStringBuilder.ConnectionString;
        }
    }
}
