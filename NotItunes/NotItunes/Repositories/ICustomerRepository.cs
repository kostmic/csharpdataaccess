﻿using NotItunes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotItunes.Repositories
{
    interface ICustomerRepository
    {
        public Customer GetCustomerById(int id);
        public Customer GetSpecificCustomerByName(string name);
        public List<Customer> PageOfCustomers(int limit, int offset);
        public List<Customer> GetAllCustomers();
        public bool AddCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public List<CustomerCountry> ReturnCustomersByCountry();
        public List<CustomerSpender> ReturnCustomersByInvoiceTotal();
        public List<CustomerGenre> ReturnMostPopularGenre(Customer customer);

    }
}
