﻿using Microsoft.Data.SqlClient;
using NotItunes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotItunes.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Used to return list of all customers in DB. Customers are returned in a List with each customer accessible as a customer object.
        /// </summary>
        /// <returns></returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();
            string sqlQuery = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConncetionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.ID = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? "MISSING POSTALCODE" : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? "MISSING PHONE" : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? "MISSING EMAIL" : reader.GetString(6);

                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return customers;

        }
        /// <summary>
        /// Takes id as parameter and returns a customer object.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Customer GetCustomerById(int id)
        {
            Customer customer = new Customer();

            using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConncetionString()))
            {
                connection.Open();
                string sqlQuery = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                    $"FROM Customer " +
                    $"WHERE CustomerId={id}";
                try
                {

                    using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.ID = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }

                }
                catch 
                {
                }
                return customer;
            }
        }
        /// <summary>
        /// Takes name as paramterer and returns a customer object.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Customer GetSpecificCustomerByName(string name)
        {
            Customer customer = new Customer();
            using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConncetionString()))
            {
                connection.Open();
                string sqlQuery = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                    $"FROM Customer " +
                    $"WHERE FirstName " +
                    $"LIKE '%{name}%'";

                try
                {
                    using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.ID = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? "MISSING POSTALCODE" : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? "MISSING PHONE" : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? "MISSING EMAIL" : reader.GetString(6);
                            }
                        }
                    }
                }
                catch
                {
                }
                return customer;
            }
        }
        /// <summary>
        /// Takes limit and offset as a parameter and returns a list of customer objects. Limit determines how many rows are returned and offset determines at which
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public List<Customer> PageOfCustomers(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();
            using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConncetionString()))
            {
                connection.Open();
                string sqlQuery = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                    $"FROM Customer " +
                    $"ORDER BY CustomerID " +
                    $"OFFSET {offset} ROWS " +
                    $"FETCH NEXT {limit} ROWS ONLY";

                using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            try
                            {
                                Customer customer = new Customer();
                                customer.ID = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? "MISSING POSTALCODE" : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? "MISSING PHONE" : reader.GetString(5);
                                customer.Email = reader.IsDBNull(6) ? "MISSING EMAIL" : reader.GetString(6);
                                customers.Add(customer);
                            }
                            catch
                            {
                            }
                        }
                        return customers;
                    }
                }
            }
        }
        /// <summary>
        /// Takes a customer object as a parameter. 
        /// Customers are added to the Customer table of the database and a bool true is returned if the insert was successful. False is returned if the insert fails
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool AddCustomer(Customer customer)
        {
            bool success = false;
            using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConncetionString()))
            {
                connection.Open();
                string sqlQuery = $"INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email)" +
                    $"VALUES (@newFirstName, @newLastName, @newCountry, @newPostalCode, @newPhone, @newEmail)";

                using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                {
                    command.Parameters.AddWithValue("@newFirstName", customer.FirstName);
                    command.Parameters.AddWithValue("@newLastName", customer.LastName);
                    command.Parameters.AddWithValue("@newCountry", customer.Country);
                    command.Parameters.AddWithValue("@newPostalCode", customer.PostalCode);
                    command.Parameters.AddWithValue("@newPhone", customer.Phone);
                    command.Parameters.AddWithValue("@newEmail", customer.Email);

                    success = command.ExecuteNonQuery() > 0 ? true : false;
                }
            }
            return success;
        }
        /// <summary>
        /// Takes a customer object as a parameter. The customer.ID should be set to the customer to be updated, aswell as other desired customer values. 
        /// True is returned if insert was successful. False is returned if the insert fails
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sqlQuery = $"UPDATE Customer " +
                $"SET FirstName = @FirstName, " +
            $"LastName = @LastName, Country = @Country, " +
            $"PostalCode = @PostalCode, Phone = @Phone, " +
            $"Email = @Email " +
            $"WHERE CustomerId = @CustomerId";
            using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConncetionString()))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                {
                    command.Parameters.AddWithValue("@CustomerId", customer.ID);
                    command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                    command.Parameters.AddWithValue("@LastName", customer.LastName);
                    command.Parameters.AddWithValue("@Country", customer.Country);
                    command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                    command.Parameters.AddWithValue("@Phone", customer.Phone);
                    command.Parameters.AddWithValue("@Email", customer.Email);

                    success = command.ExecuteNonQuery() > 0 ? true : false;
                }
            }
            return success;
        }
        /// <summary>
        /// Returns a list of objects with each country and their customer count.
        /// </summary>
        /// <returns></returns>
        public List<CustomerCountry> ReturnCustomersByCountry()
        {
            List<CustomerCountry> customersByCountry = new List<CustomerCountry>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConncetionString()))
                {
                    connection.Open();
                    string sqlQuery = "SELECT Country, " +
                        "COUNT(*) AS CustomerNumber " +
                        "FROM Customer " +
                        "GROUP BY Country " +
                        "ORDER BY CustomerNumber DESC";

                    using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customerCountry = new CustomerCountry();
                                customerCountry.Country = reader.GetString(0);
                                customerCountry.Customers = reader.GetInt32(1);
                                customersByCountry.Add(customerCountry);
                                {
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return customersByCountry;
        }
        /// <summary>
        /// Returns a list of CustomerSpender objects with each customer(FirstName and LastName) and their invoice total. Ordered descending
        /// </summary>
        /// <returns></returns>
        public List<CustomerSpender> ReturnCustomersByInvoiceTotal()
        {
            List<CustomerSpender> customersByInvoiceTotal = new List<CustomerSpender>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConncetionString()))
                {
                    connection.Open();
                    string sqlQuery = "SELECT FirstName, " +
                        "LastName, Total " +
                        "FROM Customer " +
                        "LEFT JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                        "ORDER BY Total DESC";

                    using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender customerSpender = new CustomerSpender();
                                customerSpender.FirstName = reader.GetString(0);
                                customerSpender.LastName = reader.GetString(1);
                                customerSpender.TotalSpending = reader.GetSqlDecimal(2);
                                customersByInvoiceTotal.Add(customerSpender);
                            }
                        }
                    }
                }
            }
            catch
            {
                //LOG
            }
            return customersByInvoiceTotal;
        }
        /// <summary>
        /// Returns a list with CustomerGenre objects containing a given customers most popular Music genre. 
        /// The Firstname and Lastname of the customer is used to find their favorite genre.
        /// If there are multiple most popular genres they will all be returned
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public List<CustomerGenre> ReturnMostPopularGenre(Customer customer)
        {
            
            List<CustomerGenre> customerGenreList = new List<CustomerGenre>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConncetionString()))
                {
                    connection.Open();
                    string sqlQuery = $"SELECT TOP 1 WITH TIES Customer.FirstName AS Firstname, Customer.LastName AS Lastname, Genre.Name AS Genre, COUNT(Genre.Name) AS NumPurch " +
                        $"FROM Customer " +
                        $"INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                        $"INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                        $"INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                        $"INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                        $"WHERE FirstName = '{customer.FirstName}' AND LastName = '{customer.LastName}' " +
                        $"GROUP BY Customer.FirstName, Customer.LastName, Genre.Name " +
                        $"ORDER BY Numpurch desc";

                    using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                CustomerGenre customerGenre = new CustomerGenre();
                                customerGenre.FirstName = reader.GetString(0);
                                customerGenre.LastName = reader.GetString(1);
                                customerGenre.Genre = reader.GetString(2);
                                customerGenre.TotalPurchases = reader.GetInt32(3);
                                customerGenreList.Add(customerGenre);
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return customerGenreList;
        }
    }
}
