USE SuperheroesDb
CREATE TABLE Power_Of_Superhero (
    SuperheroId INT FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id),
	SuperpowerId INT FOREIGN KEY (SuperpowerId) REFERENCES SuperPower(Id)
);