USE SuperheroesDb
INSERT INTO Assistant(AssName, superheroId)
VALUES ('Robin', (SELECT id FROM Superhero WHERE SuperName = 'Batman')),
('Jimmy', (SELECT id FROM Superhero WHERE SuperName = 'Superman')),
('Jarvis', (SELECT id FROM Superhero WHERE SuperName = 'Iron Man'));