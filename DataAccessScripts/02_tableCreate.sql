USE SuperheroesDb
CREATE TABLE Superhero (Id int IDENTITY(1,1) PRIMARY KEY, SuperName varchar(255) NOT NULL, Alias varchar(255), Origin varchar(255));
CREATE TABLE Assistant(Id int IDENTITY(1,1) PRIMARY KEY, AssName varchar(255) NOT NULL);
CREATE TABLE SuperPower(Id int IDENTITY(1,1) PRIMARY KEY, PowerName varchar(255) NOT NULL, PowerDescription varchar(255));