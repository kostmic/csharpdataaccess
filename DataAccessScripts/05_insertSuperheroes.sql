USE SuperheroesDb
INSERT INTO Superhero (SuperName, Alias, Origin)
VALUES ('Batman', 'Bruce Wayne', 'Rich orphan'),
('Superman', 'Clark Kent', 'Alien'),
('Iron Man', 'Tony Stark', 'Rich genius'),
('Wolverine', 'Logan', 'Mutant');