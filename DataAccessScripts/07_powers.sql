USE SuperheroesDb
INSERT INTO SuperPower (PowerName, PowerDescription)
VALUES ('Batarang', 'Throw a batarang'),
('Laser eyes', 'Shoots laser from the eyes'),
('Adamantium claws', 'Claws that comes out from the fists'),
('Flight', 'The ability to fly'),
('Sturdy', 'The ability to resist heavy forces')

INSERT INTO Power_Of_Superhero (SuperheroId, SuperpowerId)
VALUES ((SELECT id FROM Superhero WHERE SuperName = 'Batman'), (SELECT id FROM SuperPower WHERE PowerName = 'Batarang')),
((SELECT id FROM Superhero WHERE SuperName = 'Superman'), (SELECT id FROM SuperPower WHERE PowerName = 'Laser Eyes')),
((SELECT id FROM Superhero WHERE SuperName = 'Superman'), (SELECT id FROM SuperPower WHERE PowerName = 'Flight')),
((SELECT id FROM Superhero WHERE SuperName = 'Superman'), (SELECT id FROM SuperPower WHERE PowerName = 'Sturdy')),
((SELECT id FROM Superhero WHERE SuperName = 'Iron Man'), (SELECT id FROM SuperPower WHERE PowerName = 'Flight')),
((SELECT id FROM Superhero WHERE SuperName = 'Iron Man'), (SELECT id FROM SuperPower WHERE PowerName = 'Sturdy')),
((SELECT id FROM Superhero WHERE SuperName = 'Wolverine'), (SELECT id FROM SuperPower WHERE PowerName = 'Adamantium claws'));