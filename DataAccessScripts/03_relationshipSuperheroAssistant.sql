USE SuperheroesDb
ALTER TABLE dbo.Assistant 
ADD superheroId INT NULL;

ALTER TABLE dbo.Assistant 
ADD CONSTRAINT FK_Superhero_Assistant FOREIGN KEY (superheroId) 
REFERENCES dbo.Superhero(Id)
